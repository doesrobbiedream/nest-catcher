import { ExceptionsDictionary } from './declarations/types'
import { ExceptionFilter, Provider } from '@nestjs/common'
import { APP_FILTER } from '@nestjs/core'
import { HttpExceptionsFilter } from './filters/http-exceptions.filter'
import { MicroserviceExceptionsFilter } from './filters/microservice-exceptions.filter'

export class NestCatcher {
  public static HttpFilter(
    dictionaries?: ExceptionsDictionary | ExceptionsDictionary[]
  ): Provider<ExceptionFilter> {
    return {
      provide: APP_FILTER,
      useFactory: () => new HttpExceptionsFilter(dictionaries || []),
    }
  }

  public static MicroserviceFilter(
    dictionaries?: ExceptionsDictionary | ExceptionsDictionary[]
  ): Provider<ExceptionFilter> {
    return {
      provide: APP_FILTER,
      useFactory: () => new MicroserviceExceptionsFilter(dictionaries || []),
    }
  }
}
