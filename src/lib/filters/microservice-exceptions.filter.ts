import {
  ArgumentsHost,
  Catch,
  Logger,
  RpcExceptionFilter,
} from '@nestjs/common'
import { ExceptionsDictionary } from '../declarations/types'
import { DefaultDictionary } from '../declarations/default.dictionary'
import { ExceptionsTranslator } from '../declarations/exceptions-translator.class'
import { throwError } from 'rxjs'
import { RpcException } from '../exceptions/rpc-exception'

@Catch()
export class MicroserviceExceptionsFilter
  implements RpcExceptionFilter<RpcException>
{
  protected dictionary: ExceptionsDictionary

  constructor(dictionaries: ExceptionsDictionary | ExceptionsDictionary[]) {
    dictionaries = Array.isArray(dictionaries) ? dictionaries : [dictionaries]
    this.dictionary = dictionaries.reduce(
      (aggregated, dictionary) => ({ ...aggregated, ...dictionary }),
      DefaultDictionary
    )
  }

  catch(exception: any, host: ArgumentsHost): any {
    if (process.env.NODE_ENV === 'development') {
      Logger.debug(JSON.stringify(exception))
    }
    const ctx = host.switchToRpc()
    const pattern = (ctx.getContext().args as Array<string>).join(' ')
    const exceptionArguments = ExceptionsTranslator.translateException(
      exception
    )(this.dictionary)
    const excp = new RpcException(exceptionArguments)
    excp.request_path = pattern

    Logger.error(
      `${excp.status} | ${excp.exception} | ${excp.request_path} | ${excp.timestamp}`
    )
    return throwError(() => excp)
  }
}
