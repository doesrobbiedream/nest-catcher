import { ExceptionsDictionary } from '../declarations/types'
import { Request, Response } from 'express'
import { DefaultDictionary } from '../declarations/default.dictionary'
import { ArgumentsHost, Catch, ExceptionFilter, Logger } from '@nestjs/common'
import { ExceptionsTranslator } from '../declarations/exceptions-translator.class'
import { Exception } from '../exceptions/exception'

@Catch()
export class HttpExceptionsFilter implements ExceptionFilter {
  protected dictionary: ExceptionsDictionary

  constructor(dictionaries: ExceptionsDictionary | ExceptionsDictionary[]) {
    dictionaries = Array.isArray(dictionaries) ? dictionaries : [dictionaries]
    this.dictionary = dictionaries.reduce(
      (aggregated, dictionary) => ({ ...aggregated, ...dictionary }),
      DefaultDictionary
    )
  }

  catch(exception: any, host: ArgumentsHost): any {
    if (process.env.NODE_ENV === 'development') {
      Logger.debug(JSON.stringify(exception))
    }
    const ctx = host.switchToHttp()
    const response = ctx.getResponse<Response>()
    const request = ctx.getRequest<Request>()
    const exceptionArguments = ExceptionsTranslator.translateException(
      exception
    )(this.dictionary)
    const excp = new Exception(exceptionArguments)

    excp.request_path = request.url

    Logger.error(
      `${request.method} | ${excp.status} | ${excp.exception} | ${excp.request_path} | ${excp.timestamp}`
    )

    response.status(excp.status).json(excp)
  }
}
