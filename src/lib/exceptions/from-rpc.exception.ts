import { CustomException } from './custom.exception'
import { Exception } from './exception'
import { NestCatcherException } from '../declarations/types'

export class FromRpcException
  extends CustomException
  implements NestCatcherException
{
  protected exception: string

  constructor(protected rcp_exception: Exception) {
    super()
  }

  get data() {
    return this.rcp_exception.data
  }

  get translation() {
    return {
      code: this.rcp_exception.status,
      exception: this.rcp_exception.exception,
      message: this.rcp_exception.data.message,
    }
  }
}
