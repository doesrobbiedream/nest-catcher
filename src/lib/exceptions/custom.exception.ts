import { NestCatcherException } from '../declarations/types'

export abstract class CustomException implements NestCatcherException {
  protected abstract exception: string

  constructor(protected _data: unknown = {}) {}

  get exception_code() {
    return this.exception
  }

  get data(): { [key: string]: any } {
    return this._data
  }
}
