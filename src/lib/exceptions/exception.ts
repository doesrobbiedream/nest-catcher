import {
  ErrorData,
  ExceptionConstructorArguments,
  ExceptionResponse,
} from '../declarations/types'
import HttpStatusCode from '../declarations/http-status-codes.enum'

export class Exception implements ExceptionResponse {
  data: ErrorData
  exception: string
  request_path: string
  status: HttpStatusCode
  timestamp: string = new Date().toISOString()

  constructor({
    status,
    exception_code,
    message,
    extension_data,
  }: ExceptionConstructorArguments) {
    this.data = { message, ...extension_data }
    this.status = status
    this.request_path = 'UNKNOWN'
    this.exception = exception_code
  }
}
