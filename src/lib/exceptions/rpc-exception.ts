import { Exception } from './exception'
import { ExceptionTranslation } from '../declarations/types'

export class RpcException extends Exception {
  translation: ExceptionTranslation = {
    exception: this.exception,
    message: this.data.message,
    code: this.status,
  }
}
