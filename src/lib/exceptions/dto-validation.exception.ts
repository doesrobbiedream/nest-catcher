import { ValidationError } from '@nestjs/common'
import { CustomException } from './custom.exception'
import { ReducedValidationError } from '../declarations/types'

export class DtoValidationException extends CustomException {
  protected exception = 'VALIDATION_ERROR'
  protected _data: Record<string, unknown>
  protected message: string

  constructor(errors: ValidationError[], message?: string) {
    super()
    ;(this.message =
      message ||
      "Incoming data hasn't passed validation. Please check your inputs and try again"),
      (this._data = {
        errors: errors.map((e) => this.translateErrors(e)),
        input: errors.map((e) => e.target),
        message: this.message,
      })
  }

  translateErrors(e: ValidationError): ReducedValidationError {
    return {
      property: e.property,
      value: (!e.children && e.value) || '',
      children: e.children && e.children?.map((x) => this.translateErrors(x)),
      constraints: (!e.children && e.constraints) || undefined,
    }
  }
}
