import { ExceptionsDictionary } from './types'
import HttpStatusCode from './http-status-codes.enum'

export const DefaultDictionary: ExceptionsDictionary = {
  400: {
    code: HttpStatusCode.BAD_REQUEST,
    exception: 'BAD_REQUEST',
    message: 'Provided data is not acceptable.',
  },
  401: {
    code: HttpStatusCode.UNAUTHORIZED,
    exception: 'UNAUTHORIZED',
    message: 'Unauthenticated request.',
  },
  403: {
    code: HttpStatusCode.FORBIDDEN,
    exception: 'FORBIDDEN',
    message: 'Client is not authorized to perform this request.',
  },
  404: {
    code: HttpStatusCode.NOT_FOUND,
    exception: 'NOT_FOUND',
    message: 'Resource is not found.',
  },
  500: {
    code: HttpStatusCode.INTERNAL_SERVER_ERROR,
    exception: 'INTERNAL_SERVER_ERROR',
    message: 'Server has failed trying to provide a response.',
  },
  DEFAULT: {
    code: HttpStatusCode.INTERNAL_SERVER_ERROR,
    exception: 'INTERNAL_SERVER_ERROR',
    message: 'Something went wrong. Please contact support.',
  },
}
