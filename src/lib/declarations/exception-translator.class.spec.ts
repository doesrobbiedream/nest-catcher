import { CustomException } from '../exceptions/custom.exception'
import { ExceptionsTranslator } from './exceptions-translator.class'
import { ExceptionsDictionary } from './types'
import { Exception } from '../exceptions/exception'
import { DtoValidationException } from '../exceptions/dto-validation.exception'
import { FromRpcException } from '../exceptions/from-rpc.exception'
import HttpStatusCode from './http-status-codes.enum'

const TestMainDictionary: ExceptionsDictionary = {
  DEFAULT: {
    code: HttpStatusCode.INTERNAL_SERVER_ERROR,
    exception: 'INTERNAL_SERVER_ERROR',
    message: '',
  },
  TEST_EXCEPTION: {
    code: HttpStatusCode.INTERNAL_SERVER_ERROR,
    exception: 'INTERNAL_SERVER_ERROR',
    message: 'This is the Test Main Dictionary Message for Test Exception',
  },
}

const RpcDictionary: ExceptionsDictionary = {
  TEST_EXCEPTION: {
    code: HttpStatusCode.INTERNAL_SERVER_ERROR,
    exception: 'RPC_EXCEPTION_ERROR',
    message: 'This is a message from the RPC call',
  },
}

class MyCustomException extends CustomException {
  exception = 'TEST_EXCEPTION'
}

class MyPreservedException extends CustomException {
  exception = 'TEST_EXCEPTION'
  status = HttpStatusCode.BAD_REQUEST
}

describe('Exception Translator', function () {
  it('should return a SimpleException from a CustomException', () => {
    const ExceptionArgumentsBuilder = ExceptionsTranslator.translateException(
      new MyCustomException()
    )
    expect(
      new Exception(ExceptionArgumentsBuilder(TestMainDictionary))
    ).toBeInstanceOf(Exception)
  })

  it('should return a SimpleException from a DTOValidationException', () => {
    const ExceptionArgumentsBuilder = ExceptionsTranslator.translateException(
      new DtoValidationException([])
    )
    expect(
      new Exception(ExceptionArgumentsBuilder(TestMainDictionary))
    ).toBeInstanceOf(Exception)
  })

  it('should return a SimpleException from a FromRpcException', () => {
    const e = new Exception({
      status: TestMainDictionary['TEST_EXCEPTION'].code,
      exception_code: TestMainDictionary['TEST_EXCEPTION'].exception,
      message: TestMainDictionary['TEST_EXCEPTION'].message,
      extension_data: {},
    })
    const ExceptionArgumentsBuilder = ExceptionsTranslator.translateException(
      new FromRpcException(e)
    )
    expect(
      new Exception(ExceptionArgumentsBuilder(TestMainDictionary))
    ).toBeInstanceOf(Exception)
  })

  it('should keep the original translation when is an rcp exception', () => {
    const e = new Exception({
      status: RpcDictionary['TEST_EXCEPTION'].code,
      exception_code: RpcDictionary['TEST_EXCEPTION'].exception,
      message: RpcDictionary['TEST_EXCEPTION'].message,
    })
    const ExceptionArgumentsBuilder = ExceptionsTranslator.translateException(
      new FromRpcException(e)
    )
    expect(
      new Exception(ExceptionArgumentsBuilder(TestMainDictionary)).exception
    ).toEqual('RPC_EXCEPTION_ERROR')
  })

  it('should keep the status code provided and fill the rest', () => {
    const ExceptionArgumentBuilders = ExceptionsTranslator.translateException(
      new MyPreservedException()
    )
    const exception = new Exception(
      ExceptionArgumentBuilders(TestMainDictionary)
    )
    expect(exception.status).toEqual(HttpStatusCode.BAD_REQUEST)
    expect(exception.data.message).toEqual(
      'This is the Test Main Dictionary Message for Test Exception'
    )
  })
})
