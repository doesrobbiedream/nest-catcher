import {
  ExceptionConstructorArguments,
  ExceptionsDictionary,
  NestCatcherException,
} from './types'

export type ExceptionArgumentsBuilder = (
  dictionary: ExceptionsDictionary
) => ExceptionConstructorArguments

export class ExceptionsTranslator {
  public static translateException(
    exception: NestCatcherException
  ): ExceptionArgumentsBuilder {
    return (dictionary: ExceptionsDictionary) => {
      const translation =
        exception.translation || // Keep original translation
        dictionary[exception.exception_code] || // Search by exception code
        (exception.getStatus && dictionary[exception.getStatus()]) || // Search by status
        dictionary[exception.status] || // Search by status
        dictionary['DEFAULT'] // Fallback
      // Preserve originals if defined
      return {
        status: exception.status || translation.code,
        exception_code: exception.exception_code || translation.exception,
        message: exception.message || translation.message,
        extension_data: exception.data || {},
      }
    }
  }
}
