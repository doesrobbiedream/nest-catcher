import HttpStatusCodes from './http-status-codes.enum'
import HttpStatusCode from './http-status-codes.enum'

export type SerializableValue =
  | string
  | number
  | boolean
  | symbol
  | SerializableObject
  | SerializableArray
export type SerializableObject = { [key: string]: SerializableValue }
export type SerializableArray = Array<SerializableValue>

export interface ExceptionTranslation {
  code: HttpStatusCode
  exception: string
  message: string
}

export type ExceptionsDictionary = Record<string, ExceptionTranslation>

export interface ErrorData {
  message: string
}

export interface ReducedValidationError {
  property: string
  value?: any
  children?: ReducedValidationError[]
  constraints?: SerializableObject
}

export interface ValidationErrorData extends ErrorData {
  input: SerializableObject
  errors: ReducedValidationError[]
}

export interface ExceptionResponse {
  status: HttpStatusCodes
  exception: string
  timestamp: string
  request_path: string
  data: ErrorData
}

export interface NestCatcherException {
  exception_code: string
  data?: { [key: string]: any }
  translation?: ExceptionTranslation
  getStatus?: () => HttpStatusCodes
  status?: HttpStatusCodes
  message?: string
}

export interface ExceptionConstructorArguments {
  status: HttpStatusCode
  exception_code: string
  message: string
  extension_data?: SerializableObject
}
